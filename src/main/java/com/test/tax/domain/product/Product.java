package com.test.tax.domain.product;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.test.tax.constants.TaxType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "mt_prd")
@Getter
@Setter
public class Product {

	private static final String SEQ_GEN = "seq_gen_mt_prd";
	private static final String SEQ = "seq_mt_prd";

	@PrePersist
	@PreUpdate
	private void prePersist() {
		this.calculate();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = SEQ_GEN)
	@SequenceGenerator(name = SEQ_GEN, sequenceName = SEQ)
	private Long prdNo;

	private String prdName;

	private BigDecimal prdPrc;

	@Enumerated(EnumType.STRING)
	private TaxType taxType;

	private BigDecimal taxAmount;

	private BigDecimal totalAmount;

	private void calculate() {
		if (this.taxType.equals(TaxType.FOOD)) {
			this.taxAmount = this.prdPrc.multiply(new BigDecimal(10)).divide(new BigDecimal(100));

		} else if (this.taxType.equals(TaxType.TOBACCO)) {
			this.taxAmount = this.prdPrc.multiply(new BigDecimal(2)).divide(new BigDecimal(100))
					.add(new BigDecimal(10));
		} else if (this.taxType.equals(TaxType.ENTERTAINMENT)) {
			if (this.prdPrc.compareTo(new BigDecimal(0)) == 1 && this.prdPrc.compareTo(new BigDecimal(100)) == -1) {
				this.taxAmount = new BigDecimal(0);
			} else {
				this.taxAmount = this.prdPrc.subtract(new BigDecimal(100)).multiply(new BigDecimal(1))
						.divide(new BigDecimal(100));
			}
		}
		this.totalAmount = this.prdPrc.add(this.taxAmount);
	}

}
