package com.test.tax.constants;

import lombok.Getter;

@Getter
public enum TaxType {

	FOOD(1, "Food"),
	TOBACCO(2, "Tobacco"),
	ENTERTAINMENT(3, "Entertainment");
	
	TaxType(int code, String type){
		this.code = code;
		this.type = type;
	}
	
	private final int code;
	private final String type;
}

