package com.test.tax.dto.product;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductListResponseDTO {

	List<ProductResponseDTO> products = new ArrayList<>();

	private BigDecimal totalAmount = new BigDecimal(0);
	private BigDecimal totalTax = new BigDecimal(0);
	private BigDecimal grandTotal = new BigDecimal(0);
	private long totalCount = 0L;
	private int start = 0;
	private int limit = 0;

}
