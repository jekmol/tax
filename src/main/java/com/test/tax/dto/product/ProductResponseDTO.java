package com.test.tax.dto.product;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponseDTO {

	private String prdName;
	private BigDecimal prdPrc;
	private BigDecimal taxAmount;
	private BigDecimal totalAmount;
	private int taxCode;
	private String type;
	
	public ProductResponseDTO() {
		this.prdName = "";
		this.prdPrc = new BigDecimal(0);
		this.taxAmount = new BigDecimal(0);
		this.totalAmount = new BigDecimal(0);
		this.taxCode = 1;
		this.type = "";
	}
	
}
