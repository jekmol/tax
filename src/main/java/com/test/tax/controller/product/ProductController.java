package com.test.tax.controller.product;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.test.tax.constants.TaxType;
import com.test.tax.domain.product.Product;
import com.test.tax.dto.product.ProductCreateDTO;
import com.test.tax.dto.product.ProductListResponseDTO;
import com.test.tax.dto.product.ProductResponseDTO;
import com.test.tax.repository.product.ProductRepository;

import io.swagger.annotations.Api;

@RestController

@Api(value = "/prd", description = "Product Operation Controller")
@RequestMapping("/prd")
public class ProductController {

	private final ProductRepository productRepository;

	private final ModelMapper mapper;

	@Autowired
	public ProductController(ProductRepository productRepository, ModelMapper mapper) {
		this.productRepository = productRepository;
		this.mapper = mapper;
	}
	
	@PostMapping("/save")
	public Object save(@RequestBody ProductCreateDTO product) {

		Product saveProduct = mapper.map(product, Product.class);
		ProductResponseDTO returnBo;
		if (product.getTaxParam() == 1) {
			saveProduct.setTaxType(TaxType.FOOD);
		} else if (product.getTaxParam() == 2) {
			saveProduct.setTaxType(TaxType.TOBACCO);
		} else if (product.getTaxParam() == 3) {
			saveProduct.setTaxType(TaxType.ENTERTAINMENT);
		} else
			returnBo = new ProductResponseDTO();

		Product saveRespBo = productRepository.save(saveProduct);
		returnBo = mapper.map(saveRespBo, ProductResponseDTO.class);
		returnBo.setTaxCode(saveRespBo.getTaxType().getCode());
		returnBo.setType(saveRespBo.getTaxType().getType());

		return returnBo;
	}

	@GetMapping("/findAll")
	public Object findAll() {

		List<Product> findAll = productRepository.findAll();
		ProductListResponseDTO responseListBo = new ProductListResponseDTO();

		for (Product obj : findAll) {
			ProductResponseDTO prdRespBo = mapper.map(obj, ProductResponseDTO.class);
			prdRespBo.setTaxCode(obj.getTaxType().getCode());
			prdRespBo.setType(obj.getTaxType().getType());

			responseListBo.setTotalAmount(responseListBo.getTotalAmount().add(obj.getPrdPrc()));
			responseListBo.setTotalTax(responseListBo.getTotalTax().add(obj.getTaxAmount()));
			responseListBo.setGrandTotal(responseListBo.getGrandTotal().add(obj.getTotalAmount()));

			responseListBo.setTotalCount(productRepository.count());
			responseListBo.getProducts().add(prdRespBo);
		}

		return responseListBo;
	}

	@GetMapping("/findById")
	public Object findById(@RequestParam("prdNo") long id) {

		Product prdObj = productRepository.findOne(id);
		ProductResponseDTO prdRespBo = mapper.map(prdObj, ProductResponseDTO.class);
		ProductListResponseDTO responseListBo = new ProductListResponseDTO();
		responseListBo.setTotalAmount(prdObj.getPrdPrc());
		responseListBo.setTotalTax(prdObj.getTaxAmount());
		responseListBo.setGrandTotal(prdObj.getPrdPrc());
		responseListBo.setTotalCount(1);
		responseListBo.getProducts().add(prdRespBo);

		return responseListBo;
	}

}
