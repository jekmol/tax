package com.test.tax;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.tax.constants.TaxType;
import com.test.tax.domain.product.Product;
import com.test.tax.repository.product.ProductRepository;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductTests {

	@Autowired
	private ProductRepository productRepositoryMock;
	
	@Test
	public void entertainmentFreeTax() throws Exception {
	    Product entProduct = new Product();
	    entProduct.setPrdNo(1L);
	    entProduct.setPrdName("Test Product");
	    entProduct.setPrdPrc(new BigDecimal(60L));
	    entProduct.setTaxType(TaxType.ENTERTAINMENT);
		
		Product foundEnt = productRepositoryMock.save(entProduct);
	    assertEquals(foundEnt.getTaxAmount(), new BigDecimal(0L));
	}

	@Test
	public void checkFoodTax() throws Exception {
		Product foodPrd = new Product();
		foodPrd.setPrdNo(10L);
		foodPrd.setPrdName("Test Product");
		foodPrd.setPrdPrc(new BigDecimal(10000));
		foodPrd.setTaxType(TaxType.FOOD);
		
		Product foundFoood = productRepositoryMock.save(foodPrd);
	    assertEquals(foundFoood.getTaxAmount(), new BigDecimal(1000));
	    
	    // 2.Tobacco
	    Product tobaccoProduct = new Product();
	    tobaccoProduct.setPrdNo(11L);
	    tobaccoProduct.setPrdName("Test Product");
	    tobaccoProduct.setPrdPrc(new BigDecimal(10000));
	    tobaccoProduct.setTaxType(TaxType.TOBACCO);
		
		Product foundTobacco = productRepositoryMock.save(tobaccoProduct);
	    assertEquals(foundTobacco.getTaxAmount(), new BigDecimal(210L));
	    
	    // 2.Ent
	    Product entProduct = new Product();
	    entProduct.setPrdNo(12L);
	    entProduct.setPrdName("Test Product");
	    entProduct.setPrdPrc(new BigDecimal(10000));
	    entProduct.setTaxType(TaxType.ENTERTAINMENT);
		
		Product foundEnt = productRepositoryMock.save(entProduct);
	    assertEquals(foundEnt.getTaxAmount(), new BigDecimal(99L));
		
	}
}
